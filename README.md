# DUTT

Deta Ultimate Time Tracker aka DUTT is your personnal time tracking tool.

DUTT is a Deta + web version of the CLI tool [UTT](https://github.com/larose/utt) by [Mathieu Larose](https://github.com/larose)

Thanks to [Deta](https://deta.space/), [Svelte](https://svelte.dev/) and [FastAPI](https://fastapi.tiangolo.com/) for their beautyful and powerful tools.

---

## Features

* Track tasks: project, activity, duration, exceptions
* Report today's projects total activity duration
* Report today's activities total activity duration (merge tasks wit the same descrpition)
* Report today's details of all tracked tasks

---

## Usage

* First of all, you have to say *hello* when you arrive.
* Next, you can add a new task **when it is done**, the tool track the time consumed when you have finished working on it.
  * **Project name** is optional
  * **Task description** is mandatory. If two tasks have the same description, their time is merged has the same activity (still splited in the details view)
  * **Is a break** is used to sum all the breaks of the day. Removed from the total worked time.
  * **To be ignored** must be used to track an activity out of scope of your tracked work (a call with someone, not linked to your activity, but taking time).
  * *Push down arrow* to submit the task. Focus automaticaly go back to the *Project* field.

---

## License

Under OSS [MIT License](LICENSE)

---

## Project status

Started on Feb 8th 2023, still the 1st release, the roadmap will be setup shortly.
