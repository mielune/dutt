# DUTT arrakis

---

## **arrakis.2** (2023-02-10)

### Bug Fixes

* dark and light modes.
* odd-even color switch in tables.

---

## **arrakis** (2023-02-10)

### New Features

* First release of Deta UTT
* Display today tasks report.

---
