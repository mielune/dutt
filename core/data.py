from typing import List
from context import db
from retrying import retry
from model import HELLO, OneDay, Task
from compute import iso_2_day


def __items_to_tasks(items: List[dict]) -> List[Task]:
    return [Task(**item) for item in items]


@retry(stop_max_attempt_number=5, stop_max_delay=2000, wait_fixed=300)
def fetch_all_day(day: OneDay, filter: dict = {}) -> List[Task]:
    filter["key?r"] = [day.start, day.end]
    res = db.fetch(query=filter)
    result = list()
    if res.count == 0:
        return result
    result.extend(__items_to_tasks(res.items))
    while res.last:
        res = db.fetch(last=res.last)
        result.extend(__items_to_tasks(res.items))
    return result


@retry(stop_max_attempt_number=5, stop_max_delay=2000, wait_fixed=300)
def has_hello(dt_iso: str()) -> bool:
    today: OneDay = iso_2_day(dt_iso)
    data = db.fetch(query={"key?r": [today.start, today.end], "task": HELLO})
    if data.count > 0:
        return True
    return False
