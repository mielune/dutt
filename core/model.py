from pydantic import BaseModel
from typing import Union, List

HELLO: str = "Hello"
BREAK: str = "☕️"
NO_PROJECT: str = "🔳"


class Hello(BaseModel):
    key: str  # Datetime ISO 8601 Extended Format


class TaskRequest(BaseModel):
    key: str  # Datetime ISO 8601 Extended Format
    project: Union[str, None] = None
    task: str
    is_a_break: bool = False
    is_ignored: bool = False


class TaskResponse(BaseModel):
    key: str  # Datetime ISO 8601 Extended Format
    project: Union[str, None] = None
    task: str
    is_hello: bool = False
    is_a_break: bool = False
    is_ignored: bool = False
    duration: Union[str, None] = None
    duration_sec: int = 0
    date_: str
    time_: str


class ProjectResponse(BaseModel):
    name: str
    duration: Union[str, None] = None
    duration_sec: int = 0


class ActivityResponse(BaseModel):
    name: str
    duration: Union[str, None] = None
    duration_sec: int = 0


class TasksResponse(BaseModel):
    items: List[TaskResponse] = []
    projects: List[ProjectResponse] = []
    activities: List[ActivityResponse] = []
    total: str
    working: str
    break_: str


class Task(BaseModel):
    key: str  # epoch str
    project: Union[str, None] = None
    task: str
    is_hello: bool = False
    is_a_break: bool = False
    is_ignored: bool = False


class OneDay(BaseModel):
    start: str  # epoch str
    end: str  # epoch str
