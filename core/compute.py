from datetime import datetime, timedelta
from dateutil.parser import parse
from typing import List, Union
from model import OneDay, Task, TaskResponse, ProjectResponse, BREAK, NO_PROJECT, ActivityResponse


def iso_2_epoch(iso: str) -> str:
    return str(int(parse(iso).timestamp()))


def iso_2_day(dt_iso: str) -> OneDay:
    dt = parse(dt_iso)
    today = dt.date()
    start = datetime(today.year, today.month, today.day)
    end = start + timedelta(1)
    return OneDay(start=str(int(start.timestamp())), end=str(int(end.timestamp())))


def task_2_response(task: Task, prev_key: Union[int, None]) -> TaskResponse:
    task_dt = datetime.fromtimestamp(int(task.key))
    duration = None
    duration_sec = 0
    if prev_key:
        prev_dt = datetime.fromtimestamp(prev_key)
        delta = task_dt - prev_dt
        duration = str(delta)
        duration_sec = delta.seconds
    task_date = task_dt.strftime("%Y-%m-%d")
    task_time = task_dt.strftime("%H:%M")
    return TaskResponse(**task.dict(), duration=duration, duration_sec=duration_sec, date_=task_date, time_=task_time)


def __add_to_project(res: dict, name: str, duration_sec: int):
    if name not in res:
        res[name] = ProjectResponse(name=name)
    res[name].duration_sec += duration_sec


def tasks_2_projects(tasks: List[TaskResponse]) -> List[ProjectResponse]:
    projects = dict()
    break_project = ProjectResponse(name=BREAK)
    no_project = ProjectResponse(name=NO_PROJECT)
    for task in tasks:
        if task.is_hello or task.is_ignored:
            continue
        if task.is_a_break:
            break_project.duration_sec += task.duration_sec
            continue
        if not task.project:
            no_project.duration_sec += task.duration_sec
            continue
        else:
            __add_to_project(projects, task.project, task.duration_sec)
    res = [
        ProjectResponse(**prj.dict(exclude_none=True), duration=str(timedelta(seconds=prj.duration_sec)))
        for k, prj in projects.items()
    ]
    res.sort(key=lambda x: x.name, reverse=False)
    if no_project.duration_sec > 0:
        no_project.duration = str(timedelta(seconds=no_project.duration_sec))
        res.append(no_project)
    if break_project.duration_sec > 0:
        break_project.duration = str(timedelta(seconds=break_project.duration_sec))
        res.append(break_project)
    return res


def tasks_2_activities(tasks: List[TaskResponse]) -> List[ActivityResponse]:
    activities = dict()

    return activities
