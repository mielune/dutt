import logging
from datetime import datetime
from fastapi import FastAPI, HTTPException, status
from model import HELLO, Task, TasksResponse, TaskRequest, Hello
import context as ctx
from compute import iso_2_day, iso_2_epoch, task_2_response, tasks_2_projects, tasks_2_activities, tasks_2_summary
from data import has_hello, fetch_all_day


app = FastAPI()
ctx.cors(app)
db = ctx.db


@app.get("/version")
def get_version():
    return {"version": ctx.DETA_SPACE_APP_VERSION}


@app.put("/hello")
def put_hello(hel: Hello):
    if not has_hello(hel.key):
        db.put(Task(key=iso_2_epoch(hel.key), task=HELLO, is_hello=True).dict())
    else:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)


@app.get("/hello/{dt}")
def get_hello(dt: str):
    if has_hello(dt):
        return
    else:
        raise HTTPException(status_code=status.HTTP_204_NO_CONTENT)


@app.post("/task")
def post_task(task: TaskRequest):
    data = task.dict()
    data["key"] = iso_2_epoch(data["key"])
    db.put(Task(**data).dict())


@app.get("/tasks/{dt_iso}")
def get_tasks(dt_iso: str) -> TasksResponse:
    try:
        response: TasksResponse = TasksResponse()
        today = iso_2_day(dt_iso)
        tasks = fetch_all_day(today)
        prev_key = None
        for task in tasks:
            resp = task_2_response(task, prev_key)
            prev_key = int(task.key)
            response.items.append(resp)
        response.projects = tasks_2_projects(response.items)
        response.activities = tasks_2_activities(response.items)
        response.total, response.working, response.break_ = tasks_2_summary(response.items)
        response.items.sort(key=lambda x: x.key, reverse=True)
        return response
    except Exception as ex:
        logging.exception(ex)
        raise ex


@app.get("/report")
def get_report(from_date: str = None, to_date: str = None):
    result = db.fetch()
    response = list()
    ts1 = None
    for row in result.items:
        data: Task = Task(**row)
        ts = datetime.datetime.fromtimestamp(int(data.key))
        if row["is_hello"]:
            ts1 = ts
        else:
            if not data.is_ignored:
                delta = ts - ts1
                rep = data.dict()
                rep["ts"] = ts.strftime("%Y-%m-%d %H:%M:%S")
                rep["delta_str"] = str(delta)
                rep["delta_sec"] = delta.seconds
                del rep["key"]
                del rep["is_hello"]
                del rep["is_a_break"]
                del rep["is_ignored"]
                response.append(rep)
            ts1 = ts
    return response
