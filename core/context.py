from starlette.config import Config
import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from deta import Deta

config = Config(".env")
CORS = config("CORS", cast=bool, default=None)
DETA_SPACE_APP_VERSION = config("DETA_SPACE_APP_VERSION")

deta = Deta()
db = deta.Base("track")


def cors(app: FastAPI):
    if CORS:
        logging.info("CORS * enabled")
        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
