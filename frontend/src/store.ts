import { writable } from 'svelte/store'

export const error = writable('')
export const refreshToday = writable(false)
