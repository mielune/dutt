export type TaskRequest = {
  key?: string
  project?: string
  task?: string
  is_a_break: boolean
  is_ignored: boolean
}

export type TaskResponse = {
  key: string
  project?: string
  task: string
  is_hello: boolean
  is_a_break: boolean
  is_ignored: boolean
  duration: string
  time_: string
  date_: string
}

export type ProjectResponse = {
  name: string
  duration?: string
  duration_sec: number
}

export type ActivityResponse = {
  name: string
  duration?: string
  duration_sec: number
}

export type TasksResponse = {
  items: Array<TaskResponse>
  projects: Array<ProjectResponse>
  activities: Array<ActivityResponse>
  total: string
  working: string
  break_: string
}
