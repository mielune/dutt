---
title: "DUTT: Deta Ultimate Time Tracker"
tagline: "Your personal time tracker"
theme_color: "#f26daa"
git: "https://gitlab.com/mielune/dutt"
homepage: "https://gitlab.com/mielune/dutt"
---

Deta Ultimate Time Tracker aka DUTT is your personnal time tracking tool.

Deta version of Ultimate Time Tracker app from [UTT](https://github.com/larose/utt) by [Mathieu Larose](https://github.com/larose).

Thanks to [Deta](https://deta.space/), [Svelte](https://svelte.dev/) and [FastAPI](https://fastapi.tiangolo.com/) for their beautyful and powerful tools.

Laurent
